﻿using Integracao.Catalago.Instaleap.Entities;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Integracao.Catalago.Instaleap.IHttpRepository
{
    public interface IApiInstaleap
    {
        Task<InstaleapResponse> EnviarCatalogoApi(ProductInstaleap product);
    }
}
