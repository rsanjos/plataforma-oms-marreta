﻿using FluentValidation;
using Integracao.Catalago.Instaleap.Entities;

namespace Integracao.Catalago.Instaleap.Validator
{
    public class ProductValidator : AbstractValidator<Product>
    {
        public ProductValidator()
        {
            //RuleFor(x => x.storeReference)
            //    .NotNull()
            //    .NotEmpty();

            //RuleFor(x => x.upsert)
            //    .NotNull()
            //    .NotEmpty();

            RuleFor(x => x)
                .NotNull()
                .NotEmpty();

            RuleFor(x => x.name)
                .NotNull()
                .NotEmpty();

            RuleFor(x => x.unit)
                .NotNull()
                .NotEmpty();

            //RuleFor(x => x.product.price)
            //    .NotNull()
            //    .NotEmpty();

            RuleFor(x => x.categoryReference)
                .NotNull()
                .NotEmpty();

            RuleFor(x => x.sku)
                .NotNull()
                .NotEmpty();

            RuleFor(x => x.photosUrls)
                .NotNull()
                .NotEmpty();

            RuleFor(x => x.subUnit)
                .NotNull()
                .NotEmpty();

            RuleFor(x => x.unit)
                .NotNull()
                .NotEmpty();

            RuleFor(x => x.brand)
                .NotNull()
                .NotEmpty();
        }
    }
}
