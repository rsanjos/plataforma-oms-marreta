﻿using Integracao.Catalago.Instaleap.Entities;
using Integracao.Catalago.Instaleap.IHttpRepository;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Integracao.Catalago.Instaleap.HttpRepository
{
    public  class ApiInstaleap : HttpClient, IApiInstaleap
    {
        private readonly IConfiguration _configuration;
        public ApiInstaleap(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        
        public async Task<InstaleapResponse> EnviarCatalogoApi(ProductInstaleap products)
        {
            try
            {
                string url = _configuration.GetSection("ApiInstaleap:Url").Value;
                string key = _configuration.GetSection("ApiInstaleap:Key").Value;

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, url);

                request.Headers.Add("x-api-key", key);
                
                var message = JsonConvert.SerializeObject(products);

                request.Content = new StringContent(message, Encoding.Default, "application/json");

                var response = await SendAsync(request);

                if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    return null;

                var result = JsonConvert.DeserializeObject<InstaleapResponse>(await response.Content.ReadAsStringAsync());
                result.StatusCode = response.StatusCode;

                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
    }
}
