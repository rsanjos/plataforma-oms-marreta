﻿using System.Threading.Tasks;

namespace Integracao.Catalago.Instaleap.IApplication
{
    public interface ISyncApplication
    {
        Task SyncCatalog(string loja);
    }
}
