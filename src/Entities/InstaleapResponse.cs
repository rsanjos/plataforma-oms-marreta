﻿using System.Net;

namespace Integracao.Catalago.Instaleap.Entities
{
    public  class InstaleapResponse
    {
        public string Msg { get; set; }
        public string Error { get; set; }
        public HttpStatusCode StatusCode { get; set; }
    }
}
