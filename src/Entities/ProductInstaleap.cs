﻿using System.Collections.Generic;

namespace Integracao.Catalago.Instaleap.Entities
{
    public class ProductInstaleap
    {
        public string storeReference { get; set; }
        public bool? upsert { get; set; }
        public bool othersOutOfStock { get; set; }
        public List<Product> products { get; set; }
    }

    public class Product
    {
        public string name { get; set; }
        public string sku { get; set; }
        public string photosUrls { get; set; }
        public string unit { get; set; }
        public double price { get; set; }
        public double clickMultiplier { get; set; }
        public int specialMaxQty { get; set; }
        public bool showSubUnit { get; set; }
        public double specialPrice { get; set; }
        public int subQty { get; set; }
        public string subUnit { get; set; }
        public int maxQty { get; set; }
        public int stock { get; set; }
        public string ean { get; set; }
        public long boost { get; set; }
        public string location { get; set; }
        public string search_keywords { get; set; }
        public string brand { get; set; }
        public string slug { get; set; }
        public string categoryReference { get; set; }
        public bool isActive { get; set; }
        public object formats { get; set; }
    }
}
