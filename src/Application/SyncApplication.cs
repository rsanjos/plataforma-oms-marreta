﻿using Integracao.Catalago.Instaleap.Database;
using Integracao.Catalago.Instaleap.Database.Interfaces;
using Integracao.Catalago.Instaleap.Database.Models.Mongo;
using Integracao.Catalago.Instaleap.Database.Models.Oracle;
using Integracao.Catalago.Instaleap.Entities;
using Integracao.Catalago.Instaleap.Helper;
using Integracao.Catalago.Instaleap.IApplication;
using Integracao.Catalago.Instaleap.IHttpRepository;
using Integracao.Catalago.Instaleap.Validator;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Integracao.Catalago.Instaleap.Application
{
    public class SyncApplication : ISyncApplication
    {
        private readonly IOracleRepository _oracleRepository;
        private readonly IApiInstaleap _apiInstaleap;
        private readonly ProductValidator _productValidator;
        private readonly IMongoRepository _mongoRepository;
        private readonly IConfiguration _configuration;

        public SyncApplication(
            IOracleRepository oracleRepository, IApiInstaleap apiInstaleap, IMongoRepository mongoRepository, IConfiguration configuration)
        {
            _oracleRepository = oracleRepository;
            _mongoRepository = mongoRepository;
            _apiInstaleap = apiInstaleap;
            _productValidator = new ProductValidator();
            _configuration = configuration;
        }

        public async Task SyncCatalog(string loja)
        {
            try
            {
                Console.WriteLine($"Inicio sync catalogo - [{DateTime.Now}]");
                Stopwatch stopwatch = new Stopwatch();

                List<ProductParsed> produtos = new ();

                //Loja = 0 | Executa catálogo de promoções
                if (loja.Equals("0"))
                {
                    stopwatch.Start();
                    Console.WriteLine($"Buscando produtos promoção no Oracle - [{DateTime.Now}]");
                    produtos = await _oracleRepository.BuscarProdutosPromocao();
                    Console.WriteLine($"Fim busca produtos promoção no Oracle - [{stopwatch.Elapsed}]");
                    stopwatch.Stop();
                }
                else
                {
                    _mongoRepository.RemoverColecao(loja);

                    stopwatch.Start();
                    Console.WriteLine($"Buscando produtos no Oracle - [{DateTime.Now}]");
                    produtos = await _oracleRepository.BuscarProdutosV2(DateTime.Now, loja);
                    Console.WriteLine($"Fim busca produtos no Oracle - [{stopwatch.Elapsed}]");
                    stopwatch.Stop();
                }

                if (produtos.Any())
                {
                    if (loja.Equals("0"))
                    {
                        foreach (var itens in produtos.GroupBy(x => x.StoreReference))
                        {
                            var produtosPorLoja = itens.ToList();

                            stopwatch.Start();
                            Console.WriteLine($"Enviando {itens.Count()} produtos para a API Instaleap.");
                            produtosPorLoja.RemoveAll(x => x == null);
                            var dadosApi = MapearDadosParaApi(produtosPorLoja);
                            
                            var lojaPromo = string.Concat(dadosApi.storeReference.Replace("Oba-", ""), "_Promo");
                            _mongoRepository.RemoverColecao(lojaPromo);
                            
                            var responseApi = await EnviarProductParaInstaleap(dadosApi, lojaPromo);
                        }
                    }
                    else
                    {
                        stopwatch.Start();
                        Console.WriteLine($"Enviando {produtos.Count} produtos para a API Instaleap.");
                        produtos.RemoveAll(x => x == null);
                        var dadosApi = MapearDadosParaApi(produtos);
                        var responseApi = await EnviarProductParaInstaleap(dadosApi, dadosApi.storeReference);
                    }
                }
                else
                {
                    Console.WriteLine("Não foi encontrado produtos.");
                    return;
                }

                Console.WriteLine($"Tempo processamento: {stopwatch.Elapsed}");
                stopwatch.Stop();
                return;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public async Task GravarLogMongo(string skuId, string nomeProduto, bool status, string response, string request, string loja, Exception ex = null, double clickMultiplier = 0, string availability = null)
        {
            var logInstaleap = new LogInstaleap()
            {
                SkuId = skuId,
                NomeProduto = nomeProduto,
                Availability = availability,
                ClickMultiplier = clickMultiplier,
                Request = request,
                Response = response,
                Status = status,
                Exception = ex?.Message,
                UpdatedAt = DateTime.UtcNow
            };

            await _mongoRepository.AtualizarSkusEnviados(logInstaleap, loja);

        }

        private async Task<bool> EnviarProductParaInstaleap(ProductInstaleap dadosApi, string loja)
        {
            var tamanhoLote = _configuration.GetSection("LoteEnvioApi").Value;
            int pageSize = Convert.ToInt32(tamanhoLote) > 0 ? Convert.ToInt32(tamanhoLote) : 200;

            var batch = dadosApi.products.Batch(pageSize);
            var tamanho = batch.Count();

            for ( int page = 0; page < tamanho; page++)
            {
                var products = dadosApi.products.Skip(page * pageSize).Take(pageSize).ToList();

                var body = new ProductInstaleap()
                {
                    othersOutOfStock = dadosApi.othersOutOfStock,
                    storeReference = dadosApi.storeReference,
                    upsert = dadosApi.upsert,
                    products = new List<Product>()
                };

                foreach (var product in products)
                {
                    var request = JsonConvert.SerializeObject(product);
                    var validation = await _productValidator.ValidateAsync(product);
                    if (validation.Errors.Any())
                    {
                        var erro = JsonConvert.SerializeObject(validation.Errors);
                        await GravarLogMongo(product.sku, product.name, false, erro, request, loja);
                        continue;
                    }

                    body.products.Add(product);
                }

                var result = await _apiInstaleap.EnviarCatalogoApi(body);

                if(result == null)
                    await GravarLogMongoErroApi(false, "Erro Api Instaleap", JsonConvert.SerializeObject(body), loja);
                else if (result?.StatusCode == System.Net.HttpStatusCode.OK)
                    await GravarLogMongoSucesso(products, loja);
                //await GravarLogMongoBulk(products, loja);
                else
                    await GravarLogMongoErroApi(false, result.Error, JsonConvert.SerializeObject(body), loja);
            }

            return true;
        }

        private async Task GravarLogMongoSucesso(List<Product> products, string loja)
        {
            var logsInstaleap = products.Select(x => new LogInstaleap()
            {
                SkuId = x.sku,
                NomeProduto = x.name,
                ClickMultiplier = x.clickMultiplier,
                Request = JsonConvert.SerializeObject(x),
                Status = true,
                UpdatedAt = DateTime.UtcNow
            }).ToList();

            await _mongoRepository.AtualizarSkusEnviadosSucesso(logsInstaleap, loja);
        }

        private async Task GravarLogMongoErroApi(bool status, string error, string request, string loja)
        {
            var logInstaleap = new LogInstaleap()
            {
                Request = request,
                Response = error,
                Status = status,
                UpdatedAt = DateTime.UtcNow
            };

            await _mongoRepository.AtualizarSkusEnviadosErro(logInstaleap, loja);
        }

        private async Task GravarLogMongoBulk(List<Product> products, string loja)
        {
            await _mongoRepository.AtualizarSkusEnviadosBulk(products, loja);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        private ProductInstaleap MapearDadosParaApi(List<ProductParsed> produtos)
        {
            var instaleap = new ProductInstaleap()
            {
                othersOutOfStock = false,
                storeReference = produtos.FirstOrDefault().StoreReference,
                upsert = true,
                products = new List<Product>()
            };

            foreach (var produto in produtos)
            {
                if (string.IsNullOrEmpty(produto.Brand) && (produto.CategoryReference1 == "21728" || produto.CategoryReference2 == "21728" || produto.CategoryReference3 == "21728"))
                {
                    instaleap.products.Add(new Product()
                    {
                        name = produto.Name,
                        sku = produto.Sku,
                        photosUrls = produto.PhotosUrls != null ? produto.PhotosUrls : "https://obahortifruti.com.br/wp-content/uploads/2019/10/nao-disponivel.jpg",
                        unit = produto.Unit,
                        price = produto.Price,
                        clickMultiplier = produto.ClickMultiplier > 0 ? produto.ClickMultiplier : 1,
                        specialMaxQty = Convert.ToInt32(produto.SpecialMaxQty),
                        showSubUnit = produto.ShowSubUnit == "true",
                        specialPrice = produto.SpecialPrice,
                        subQty = Convert.ToInt32(produto.SubQty),
                        subUnit = produto.Unit.ToUpper() == "UN" ? produto.Unit : produto.Unit.ToUpper() == "KG" ? "g" : produto.Unit,
                        maxQty = Convert.ToInt32(produto.MaxQty),
                        stock = produto.Stock,
                        ean = produto.EAN == null || produto.EAN == "" ? " " : produto.EAN,
                        boost = Convert.ToInt64(produto.Boost),
                        location = produto.Location,
                        search_keywords = produto.SearchKeywords.Length > 255 ? produto.SearchKeywords.Substring(0, 254) : produto.SearchKeywords,
                        brand = "FLV",
                        slug = produto.Slug != null ? produto.Slug : "",
                        categoryReference = string.IsNullOrEmpty(produto.CategoryReference1) ? "outros" : produto.CategoryReference1,
                        isActive = true,
                        formats = Array.Empty<string>()
                    });
                }
                else
                {
                    instaleap.products.Add(new Product()
                    {
                        name = produto.Name,
                        sku = produto.Sku,
                        photosUrls = produto.PhotosUrls != null ? produto.PhotosUrls : "https://obahortifruti.com.br/wp-content/uploads/2019/10/nao-disponivel.jpg",
                        unit = produto.Unit,
                        price = produto.Price,
                        clickMultiplier = produto.ClickMultiplier > 0 ? produto.ClickMultiplier : 1,
                        specialMaxQty = Convert.ToInt32(produto.SpecialMaxQty),
                        showSubUnit = produto.ShowSubUnit == "true",
                        specialPrice = produto.SpecialPrice,
                        subQty = Convert.ToInt32(produto.SubQty),
                        subUnit = produto.Unit.ToUpper() == "UN" ? produto.Unit : produto.Unit.ToUpper() == "KG" ? "g" : produto.Unit,
                        maxQty = Convert.ToInt32(produto.MaxQty),
                        stock = produto.Stock,
                        ean = produto.EAN == null || produto.EAN == "" ? " " : produto.EAN,
                        boost = Convert.ToInt64(produto.Boost),
                        location = produto.Location,
                        search_keywords = produto.SearchKeywords.Length > 255 ? produto.SearchKeywords.Substring(0, 254) : produto.SearchKeywords,
                        brand = produto.Brand,
                        slug = produto.Slug != null ? produto.Slug : "",
                        categoryReference = string.IsNullOrEmpty(produto.CategoryReference1) ? "outros" : produto.CategoryReference1,
                        isActive = true,
                        formats = Array.Empty<string>()
                    });
                }
            }

            return instaleap;
        }

        private List<Product> GetPaginatedProduct(List<Product> list, int page, int pageSize)
        {
            return list.Skip(page * pageSize).Take(pageSize).ToList();
        }
    }
}