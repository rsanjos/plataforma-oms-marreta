﻿using Integracao.Catalago.Instaleap.IApplication;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace Integracao.Catalago.Instaleap
{
    public class Program
    {
        static async Task Main(string[] args)
        {
            if (args.Length > 0)
            {
                IServiceCollection services = new ServiceCollection();
                Startup startup = new Startup();

                startup.ConfigureServices(services);

                IServiceProvider serviceProvider = services.BuildServiceProvider();

                var loja = args[0];
                var service = serviceProvider.GetService<ISyncApplication>();
                await service.SyncCatalog(loja);
            }
            else
                Console.WriteLine("Informar o número da loja no parâmetro.");
        }
    }
}
