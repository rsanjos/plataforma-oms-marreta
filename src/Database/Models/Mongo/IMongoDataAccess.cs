﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Integracao.Catalago.Instaleap.Database.Models.Mongo
{
    public interface IMongoDataAccess
    {
        Task<TDocument> GetAsync<TDocument>(string collectionName, FilterDefinition<TDocument> filter);
        Task<List<TDocument>> ListAsync<TDocument>(string collectionName, FilterDefinition<TDocument> query, FindOptions<TDocument> options = null);
        Task<TDocument> GetAndUpdateAsync<TDocument>(string collectionName, Expression<Func<TDocument, bool>> filter, UpdateDefinition<TDocument> update, FindOneAndUpdateOptions<TDocument> options = null);
        Task InsertAsync<TDocument>(string collectionName, TDocument document);
        Task UpdateAsync<TDocument>(string collectionName, TDocument document, FilterDefinition<TDocument> filter);
        Task InsertOrUpdateAsync<TDocument>(string collectionName, TDocument document, FilterDefinition<TDocument> filter);
        Task DeleteOneAsync<TDocument>(string collectionName, FilterDefinition<TDocument> filter);
        IMongoDatabase GetDatabase();
        Task BulkInsert(string collectionName, IEnumerable<LogInstaleap> loteEnviado);
        Task InsertManyAsync<TDocument>(string collectionName, List<TDocument> documents);
        void DropCollection(string collectionLoja);
    }
}
