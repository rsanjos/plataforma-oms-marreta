﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Integracao.Catalago.Instaleap.Database.Models.Mongo
{
    public class SkusAtualizar
    {
        [BsonId]
        public ObjectId Id { get; set; } 
        public string Sku { get; set; }
        public bool Enviado { get; set; }
    }
}
