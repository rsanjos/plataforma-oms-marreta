﻿using System;

namespace Integracao.Catalago.Instaleap.Database.Models.Mongo
{
    public class LogInstaleap
    {
        public string SkuId { get; set; }
        public string Request { get; set; }
        public string Response { get; set; }
        public string NomeProduto { get; set; }
        public bool Status { get; set; }
        public string Exception { get; set; }
        public double ClickMultiplier { get; set; }
        public string Availability { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
