﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using MongoDB.Driver.Core.Events;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Integracao.Catalago.Instaleap.Database.Models.Mongo
{
    public class MongoDataAccess : IMongoDataAccess
    {
        private readonly IMongoSettings mongoSettings;
        private readonly IMongoDatabase database;

        private string lastCommand = null;

        public MongoDataAccess(IMongoSettings mongoSettings)
        {
            this.mongoSettings = mongoSettings ?? throw new ArgumentNullException(nameof(mongoSettings));

            database = GetDatabase();

            var conventionPack = new ConventionPack { new IgnoreExtraElementsConvention(true) };
            ConventionRegistry.Register("IgnoreExtraElements", conventionPack, type => true);
        }

        public async Task<TDocument> GetAsync<TDocument>(string collectionName, FilterDefinition<TDocument> filter)
        {
            var stopwatch = Stopwatch.StartNew();
            var operation = "GetAsync";
            try
            {
                var collection = GetCollection<TDocument>(collectionName);
                var result = await collection.Find(filter).FirstOrDefaultAsync();

                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<List<TDocument>> ListAsync<TDocument>(string collectionName, FilterDefinition<TDocument> filter, FindOptions<TDocument> options = null)
        {
            var stopwatch = Stopwatch.StartNew();
            var operation = "ListAsync";
            try
            {
                var collection = GetCollection<TDocument>(collectionName);
                var listCursor = await collection.FindAsync(filter, options);
                var result = await listCursor.ToListAsync();

                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<TDocument> GetAndUpdateAsync<TDocument>(string collectionName, Expression<Func<TDocument, bool>> filter,
            UpdateDefinition<TDocument> update, FindOneAndUpdateOptions<TDocument> options = null)
        {
            var stopwatch = Stopwatch.StartNew();
            var operation = "GetAndUpdateAsync";
            try
            {
                var collection = GetCollection<TDocument>(collectionName);
                var result = await collection.FindOneAndUpdateAsync(filter, update, options);

                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task InsertAsync<TDocument>(string collectionName, TDocument document)
        {
            var stopwatch = Stopwatch.StartNew();
            var operation = "InsertAsync";
            try
            {
                var collection = GetCollection<TDocument>(collectionName);
                await collection.InsertOneAsync(document);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task InsertManyAsync<TDocument>(string collectionName, List<TDocument> documents)
        {
            try
            {
                var collection = GetCollection<TDocument>(collectionName);
                await collection.InsertManyAsync(documents);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task UpdateAsync<TDocument>(string collectionName, TDocument document, FilterDefinition<TDocument> filter)
        {
            var stopwatch = Stopwatch.StartNew();
            var operation = "UpdateAsync";
            try
            {
                var collection = GetCollection<TDocument>(collectionName);
                await collection.ReplaceOneAsync(filter, document, new ReplaceOptions { IsUpsert = true });

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task InsertOrUpdateAsync<TDocument>(string collectionName, TDocument document, FilterDefinition<TDocument> filter)
        {
            var stopwatch = Stopwatch.StartNew();
            var operation = "InsertOrUpdateAsync";
            try
            {
                var collection = GetCollection<TDocument>(collectionName);
                await collection.ReplaceOneAsync(filter, document, new ReplaceOptions { IsUpsert = true });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task DeleteOneAsync<TDocument>(string collectionName, FilterDefinition<TDocument> filter)
        {
            var stopwatch = Stopwatch.StartNew();
            var operation = "DeleteOneAsync";

            try
            {
                var collection = GetCollection<TDocument>(collectionName);
                await collection.DeleteOneAsync(filter);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task BulkInsert(string collectionName, IEnumerable<LogInstaleap> loteEnviado)
        {
            try
            {
                var collection = GetCollection<LogInstaleap>(collectionName);

                var bulkUpdate = new List<WriteModel<LogInstaleap>>();

                foreach (var produto in loteEnviado)
                {
                    var filter = Builders<LogInstaleap>.Filter.Eq(x => x.SkuId, produto.SkuId);
                    var update = Builders<LogInstaleap>.Update.Set(x => x.ClickMultiplier, produto.ClickMultiplier)
                                                              .Set(x => x.Request, produto.Request)
                                                              .Set(x => x.NomeProduto, produto.NomeProduto)
                                                              .Set(x => x.Status, produto.Status)
                                                              .Set(x => x.UpdatedAt, produto.UpdatedAt);

                    var upsert = new UpdateOneModel<LogInstaleap>(filter, update) { IsUpsert = true };
                    bulkUpdate.Add(upsert);
                }

                await collection.BulkWriteAsync(bulkUpdate);

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #region Mongo Configurations

        //private static void SetMongoConfigurations()
        //{
        //    var pack = new ConventionPack
        //    {
        //        new GuidAsStringRepresentationConvention()
        //    };

        //    ConventionRegistry.Register(
        //        "GUIDs as strings Conventions",
        //        pack,
        //        type => type.Namespace.StartsWith(typeof(MongoDataAccess).Namespace));
        //}

        #endregion

        #region Auxiliar Methods
        public void DropCollection(string collection)
        {
            database.DropCollection(collection);
        }
        public IMongoDatabase GetDatabase()
        {
            var mongoConnectionUrl = new MongoUrl(mongoSettings.ConnectionString);
            var mongoClientSettings = MongoClientSettings.FromUrl(mongoConnectionUrl);
            mongoClientSettings.ClusterConfigurator = cb =>
            {
                cb.Subscribe<CommandStartedEvent>(e =>
                {
                    lastCommand = $"{e.CommandName} - {e.Command.ToJson()}";
                });
            };

            var client = new MongoClient(mongoClientSettings);
            return client.GetDatabase(mongoSettings.Database);
        }

        private IMongoCollection<TDocument> GetCollection<TDocument>(string collectionName)
        {
            return database.GetCollection<TDocument>(collectionName);
        }
        #endregion
    }

}
