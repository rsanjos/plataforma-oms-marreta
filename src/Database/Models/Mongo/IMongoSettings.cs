﻿namespace Integracao.Catalago.Instaleap.Database.Models.Mongo
{
    public interface IMongoSettings
    {
        string ConnectionString { get; }
        string Database { get; }
        string GetCollectionName(MongoCollections mongoCollection);
    }
}
