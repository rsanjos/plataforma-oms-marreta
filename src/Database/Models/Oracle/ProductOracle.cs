﻿using System;

namespace Integracao.Catalago.Instaleap.Database.Models.Oracle
{
    public class ProductOracle
    {
        public string storeReference { get; set; }
        public string upsert { get; set; }
        public string othersOutOfStock { get; set; }
        public string name { get; set; }
        public string sku { get; set; }
        public string photosUrls { get; set; }
        public string unit { get; set; }
        public double price { get; set; }
        public double clickMultiplier { get; set; }
        public string specialMaxQty { get; set; }
        public string showSubUnit { get; set; }
        public double specialPrice { get; set; }
        public string subQty { get; set; }
        public string subUnit { get; set; }
        public string maxQty { get; set; }
        public int stock { get; set; }
        public string EAN { get; set; }
        public string boost { get; set; }
        public string location { get; set; }
        public string search_keywords { get; set; }
        public string brand { get; set; }
        public string slug { get; set; }
        public string categoryReference1 { get; set; }
        public string categoryReference2 { get; set; }
        public string categoryReference3 { get; set; }
        public string formats { get; set; }
        public DateTime dt_cadastro { get; set; }
        public string dt_alteracao { get; set; }
        public string availability { get; set; }

        public ProductParsed ConvertToDomain()
        {
            return new ProductParsed(
                storeReference,
                upsert,
                othersOutOfStock,
                name,
                sku,
                photosUrls,
                unit,
                price,
                clickMultiplier,
                specialMaxQty,
                showSubUnit,
                specialPrice,
                subQty,
                subUnit,
                maxQty,
                stock,
                EAN,
                boost,
                location,
                search_keywords,
                brand,
                slug,
                categoryReference1,
                categoryReference2,
                categoryReference3,
                formats,
                dt_cadastro,
                dt_alteracao,
                availability
                );
        }
    }
}
