﻿using System;

namespace Integracao.Catalago.Instaleap.Database.Models.Oracle
{
    public class ProductParsed
    {
        public string StoreReference { get; set; }
        public string Upsert { get; set; }
        public string OthersOutOfStock { get; set; }
        public string Name { get; set; }
        public string Sku { get; set; }
        public string PhotosUrls { get; set; }
        public string Unit { get; set; }
        public double Price { get; set; }
        public double ClickMultiplier { get; set; }
        public string SpecialMaxQty { get; set; }
        public string ShowSubUnit { get; set; }
        public double SpecialPrice { get; set; }
        public string SubQty { get; set; }
        public string SubUnit { get; set; }
        public string MaxQty { get; set; }
        public int Stock { get; set; }
        public string EAN { get; set; }
        public string Boost { get; set; }
        public string Location { get; set; }
        public string SearchKeywords { get; set; }
        public string Brand { get; set; }
        public string Slug { get; set; }
        public string CategoryReference1 { get; set; }
        public string CategoryReference2 { get; set; }
        public string CategoryReference3 { get; set; }
        public string Formats { get; set; }
        public DateTime dt_cadastro { get; set; }
        public string dt_alteracao { get; set; }
        public string Availability { get; set; }

        public ProductParsed(
            string storeReference,
            string upsert,
            string othersOutOfStock,
            string name,
            string sku,
            string photoUrls,
            string unit,
            double price,
            double clickMultiplier,
            string specialMaxQty,
            string showSubUnit,
            double specialPrice,
            string subQty,
            string subUnit,
            string maxQty,
            int stock,
            string ean,
            string boost,
            string location,
            string searchKeywords,
            string brand,
            string slug,
            string categoryReference1,
            string categoryReference2,
            string categoryReference3,
            string formats,
            DateTime dt_cadastro,
            string dt_alteracao,
            string availability
            )
        {
            this.StoreReference = storeReference;
            this.Upsert = upsert;
            this.OthersOutOfStock = othersOutOfStock;
            this.Name = name;
            this.Sku = sku;
            this.PhotosUrls = photoUrls;
            this.Unit = unit;
            this.Price = price;
            this.ClickMultiplier = clickMultiplier;
            this.SpecialMaxQty = specialMaxQty;
            this.ShowSubUnit = showSubUnit;
            this.SpecialPrice = specialPrice;
            this.SubQty = subQty;
            this.SubUnit = subUnit;
            this.MaxQty = maxQty;
            this.Stock = stock;
            this.EAN = ean;
            this.Boost = boost;
            this.Location = location;
            this.SearchKeywords = searchKeywords;
            this.Brand = brand;
            this.Slug = slug;
            this.CategoryReference1 = categoryReference1;
            this.CategoryReference2 = categoryReference2;
            this.CategoryReference3 = categoryReference3;
            this.Formats = formats;
            this.dt_cadastro = dt_cadastro;
            this.dt_alteracao = dt_alteracao;
            this.Availability = availability;
        }
    }
}