﻿using Integracao.Catalago.Instaleap.Database.Models.Oracle;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Integracao.Catalago.Instaleap.Database.Interfaces
{
    public interface IOracleRepository
    {
        Task<List<ProductParsed>> BuscarProdutos(DateTime data, string loja);
        Task<List<ProductParsed>> BuscarProdutosPorSku(string sku, string loja);
        Task<List<ProductParsed>> BuscarProdutosV2(DateTime data, string loja);
        Task<List<ProductParsed>> BuscarProdutosPromocao();
    }
}
