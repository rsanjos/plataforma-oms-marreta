﻿using Integracao.Catalago.Instaleap.Database.Models.Mongo;
using Integracao.Catalago.Instaleap.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Integracao.Catalago.Instaleap.Database
{
    public interface IMongoRepository
    {
        Task<List<SkusAtualizar>> ObterSkusNaoEnviados();
        Task AtualizarSkusEnviados(LogInstaleap logInstaleap, string loja);
        Task AtualizarSkusEnviadosBulk(List<Product> produtos, string loja);
        Task AtualizarSkusEnviadosErro(LogInstaleap logInstaleap, string loja);
        Task AtualizarSkusEnviadosSucesso(List<LogInstaleap> logsInstaleap, string loja);
        void RemoverColecao(string loja);
    }
}