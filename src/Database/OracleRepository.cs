﻿using Dapper;
using Integracao.Catalago.Instaleap.Database.Interfaces;
using Integracao.Catalago.Instaleap.Database.Models.Oracle;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Integracao.Catalago.Instaleap.Database
{
    public class OracleRepository : IOracleRepository
    {
        private readonly IConfiguration _configuration;

        public OracleRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string GetConnectionString()
        {
            var databaseConnectionString = _configuration.GetSection("OracleSettings:RedeOba")?.Value;

            if (string.IsNullOrEmpty(databaseConnectionString))
                throw new Exception("Falha ao buscar ConnectionString");

            return databaseConnectionString;
        }


        public async Task<List<ProductParsed>> BuscarProdutos(DateTime data, string loja)
        {
            var connection = new OracleConnection(GetConnectionString());
            await connection.OpenAsync();

            var sql = "CONSINCO.PKG_INSTALEAP.P_BUSCAPRODUTO";

            var parameters = new OracleDynamicParameters();
            parameters.Add("pr_seqproduto", null);
            parameters.Add("pr_nroempresa", loja);
            parameters.Add("pr_hour", "1");
            parameters.Add("pr_segmento", "2");
            parameters.Add("pr_dataBase", DateTime.ParseExact(data.ToString("yyyy-MM-dd"), "yyyy-MM-dd", new CultureInfo("en-US", true)));
            parameters.Add("r_result", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var reader = await connection.ExecuteReaderAsync(
                    sql: sql,
                    param: parameters,
                    commandType: CommandType.StoredProcedure,
                    commandTimeout: 120,
                    transaction: null);

            var parser = SqlMapper.Parse<ProductOracle>(reader);

            if (reader.Read())
            {
                return parser.Select(x => x.ConvertToDomain()).ToList();
            }

            return null;
        }

        public async Task<List<ProductParsed>> BuscarProdutosPorSku(string sku, string loja)
        {
            try
            {
                var connection = new OracleConnection(GetConnectionString());
                await connection.OpenAsync();

                var sql = "CONSINCO.PKG_INSTALEAP.P_BUSCAPRODUTO";

                var parameters = new OracleDynamicParameters();
                parameters.Add("pr_seqproduto", sku);
                parameters.Add("pr_nroempresa", loja);
                parameters.Add("pr_dataBase", null);
                parameters.Add("pr_segmento", "2");
                parameters.Add("pr_hour", null);
                parameters.Add("r_result", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

                var reader = await connection.ExecuteReaderAsync(
                        sql: sql,
                        param: parameters,
                        commandType: CommandType.StoredProcedure,
                        commandTimeout: 120,
                        transaction: null);


                var parser = SqlMapper.Parse<ProductOracle>(reader);

                if (reader.HasRows)
                {
                    var produto = parser.Select(x => x.ConvertToDomain()).ToList();
                    await connection.CloseAsync();
                    return produto;
                }

                await connection.CloseAsync();

                return null;
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public async Task<List<ProductParsed>> BuscarProdutosV2(DateTime data, string loja)
        {
            var connection = new OracleConnection(GetConnectionString());
            await connection.OpenAsync();

            var sql = "consinco.pkg_instaleap.p_buscaProduto_v2";

            var parameters = new OracleDynamicParameters();
            parameters.Add("pr_nroempresa", loja);
            parameters.Add("pr_dataBase", null);
            parameters.Add("r_result", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var reader = await connection.ExecuteReaderAsync(
                    sql: sql,
                    param: parameters,
                    commandType: CommandType.StoredProcedure,
                    commandTimeout: 120,
                    transaction: null);

            var parser = SqlMapper.Parse<ProductOracle>(reader);

            if (reader.Read())
            {
                return parser.Select(x => x.ConvertToDomain()).ToList();
            }

            return null;
        }

        public async Task<List<ProductParsed>> BuscarProdutosPromocao()
        {
            var connection = new OracleConnection(GetConnectionString());
            await connection.OpenAsync();

            var sql = "consinco.pkg_instaleap.p_buscaProdutoPromo";

            var parameters = new OracleDynamicParameters();
            parameters.Add("pr_nroempresa", null);
            parameters.Add("r_result", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var reader = await connection.ExecuteReaderAsync(
                    sql: sql,
                    param: parameters,
                    commandType: CommandType.StoredProcedure,
                    commandTimeout: 120,
                    transaction: null);

            var parser = SqlMapper.Parse<ProductOracle>(reader);

            if (reader.Read())
            {
                return parser.Select(x => x.ConvertToDomain()).ToList();
            }
             
            return null;
        }
    }
}
