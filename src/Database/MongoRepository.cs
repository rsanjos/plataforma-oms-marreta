﻿using Integracao.Catalago.Instaleap.Database.Models.Mongo;
using Integracao.Catalago.Instaleap.Entities;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Integracao.Catalago.Instaleap.Database
{
    public class MongoRepository : IMongoRepository
    {
        private readonly IMongoSettings mongoSettings;
        private readonly IMongoDataAccess mongoDataAccess;
        private readonly IConfiguration configuration;

        public MongoRepository(IMongoSettings mongoSettings, IMongoDataAccess mongoDataAccess, IConfiguration configuration)
        {
            this.mongoSettings = mongoSettings ?? throw new ArgumentNullException(nameof(mongoSettings));
            this.mongoDataAccess = mongoDataAccess ?? throw new ArgumentNullException(nameof(mongoDataAccess));
            this.configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        public async Task<List<SkusAtualizar>> ObterSkusNaoEnviados()
        {
            var collectionName = mongoSettings.GetCollectionName(MongoCollections.skus);
            var document = await mongoDataAccess.ListAsync(collectionName,
                Builders<SkusAtualizar>.Filter.Eq(a => a.Enviado, false));

            if (document == null)
                return null;

            return document;
        }

        public async Task AtualizarSkusEnviados(LogInstaleap logInstaleap, string loja)
        {
            var collectionName = mongoSettings.GetCollectionName(MongoCollections.intregacoes);
            var collectionLoja = collectionName + loja.Replace("Oba-", string.Empty);
            await mongoDataAccess.UpdateAsync(collectionLoja, logInstaleap,
                Builders<LogInstaleap>.Filter.Eq(a => a.SkuId, logInstaleap.SkuId));
        }

        public async Task AtualizarSkusEnviadosErro(LogInstaleap logInstaleap, string loja)
        {
            var collectionName = mongoSettings.GetCollectionName(MongoCollections.intregacoes);
            var collectionLoja = collectionName + loja.Replace("Oba-", string.Empty);
            await mongoDataAccess.InsertAsync(collectionLoja, logInstaleap);
        }

        public async Task AtualizarSkusEnviadosBulk(List<Product> produtos, string loja)
        {
            var collectionName = mongoSettings.GetCollectionName(MongoCollections.intregacoes);
            var collectionLoja = collectionName + loja.Replace("Oba-", string.Empty);

            var loteEnviado = produtos.Select(x => new LogInstaleap()
            {
                NomeProduto = x.name,
                Request = JsonConvert.SerializeObject(x),
                SkuId = x.sku,
                UpdatedAt = DateTime.Now.AddHours(-3),
                ClickMultiplier = x.clickMultiplier,
                Exception = null,
                Status = true
            });

            await mongoDataAccess.BulkInsert(collectionLoja, loteEnviado);

            //await mongoDataAccess.InsertManyAsync(collectionLoja, produtos);
        }

        public async Task AtualizarSkusEnviadosSucesso(List<LogInstaleap> logsInstaleap, string loja)
        {
            var collectionName = mongoSettings.GetCollectionName(MongoCollections.intregacoes);
            var collectionLoja = collectionName + loja.Replace("Oba-", string.Empty);

            await mongoDataAccess.InsertManyAsync(collectionLoja, logsInstaleap);
        }

        public void RemoverColecao(string loja)
        {
            var collectionName = mongoSettings.GetCollectionName(MongoCollections.intregacoes);

            var collectionLoja = collectionName + loja;

            mongoDataAccess.DropCollection(collectionLoja);
        }
    }
}
