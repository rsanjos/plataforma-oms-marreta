﻿using Integracao.Catalago.Instaleap.Application;
using Integracao.Catalago.Instaleap.Database;
using Integracao.Catalago.Instaleap.Database.Interfaces;
using Integracao.Catalago.Instaleap.Database.Models.Mongo;
using Integracao.Catalago.Instaleap.HttpRepository;
using Integracao.Catalago.Instaleap.IApplication;
using Integracao.Catalago.Instaleap.IHttpRepository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Integracao.Catalago.Instaleap
{
    public class Startup
    {
        IConfiguration Configuration { get; }

        public Startup()
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IOracleRepository, OracleRepository>();
            services.AddScoped<IMongoRepository, MongoRepository>();
            services.AddScoped<IMongoSettings, MongoSettings>();
            services.AddScoped<IMongoDataAccess, MongoDataAccess>();
            services.AddScoped<IApiInstaleap, ApiInstaleap>();
            services.AddScoped<ISyncApplication, SyncApplication>();
            //services.AddHostedService<SyncApplication>();

            services.AddSingleton(Configuration);

            services.AddAuthorization();
            services.AddAuthentication();
            //services.AddControllers();

        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //app.UseHttpsRedirection();
            //app.UseRouting();
            
            //app.UseAuthorization();

            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapControllers();
            //});
        }
    }
}
