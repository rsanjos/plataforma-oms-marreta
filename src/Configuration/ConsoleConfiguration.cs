﻿namespace Integracao.Catalago.Instaleap.Configuration
{
    public class ConsoleConfiguration
    {
        public string ConnectionString { get; set; }
        public OracleSettings Oracle { get; set; }
        public MongoSettings Mongo{ get; set; }

        public class OracleSettings 
        {
            public string RedeOba { get; set; }
        }

        public class MongoSettings
        {
            public string Name { get; set; }
            public string Database { get; set; }
            public string ConnectionString { get; set; }
        }
    }
}
